<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'Webmozart\\PathUtil\\' => array($vendorDir . '/webmozart/path-util/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Vanilla\\JsConnect\\' => array($vendorDir . '/vanilla/js-connect-php/src'),
    'Vanilla\\CloudInterops\\' => array($vendorDir . '/vanilla/cloud-interops/Vanilla/CloudInterops'),
    'Vanilla\\' => array($baseDir . '/library/Vanilla'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php74\\' => array($vendorDir . '/symfony/polyfill-php74'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Contracts\\Cache\\' => array($vendorDir . '/symfony/cache-contracts'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\VarExporter\\' => array($vendorDir . '/symfony/var-exporter'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Lock\\' => array($vendorDir . '/symfony/lock'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Cache\\' => array($vendorDir . '/symfony/cache'),
    'RobotsTxtParser\\' => array($vendorDir . '/bopoda/robots-txt-parser/src/RobotsTxtParser'),
    'Rize\\' => array($vendorDir . '/rize/uri-template/src/Rize'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Psr\\EventDispatcher\\' => array($vendorDir . '/psr/event-dispatcher/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'PHPMailer\\PHPMailer\\' => array($vendorDir . '/phpmailer/phpmailer/src'),
    'Nbbc\\' => array($vendorDir . '/vanilla/nbbc/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Mimey\\' => array($vendorDir . '/ralouphie/mimey/src'),
    'Michelf\\' => array($vendorDir . '/michelf/php-markdown/Michelf'),
    'League\\Uri\\' => array($vendorDir . '/league/uri/src', $vendorDir . '/league/uri-interfaces/src'),
    'League\\HTMLToMarkdown\\' => array($vendorDir . '/league/html-to-markdown/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Grpc\\Gcp\\' => array($vendorDir . '/google/grpc-gcp/src'),
    'Grpc\\' => array($vendorDir . '/grpc/grpc/src/lib'),
    'Google\\Type\\' => array($vendorDir . '/google/common-protos/src/Type'),
    'Google\\Rpc\\' => array($vendorDir . '/google/common-protos/src/Rpc'),
    'Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/Google/Protobuf'),
    'Google\\LongRunning\\' => array($vendorDir . '/google/longrunning/src/LongRunning'),
    'Google\\Iam\\' => array($vendorDir . '/google/common-protos/src/Iam'),
    'Google\\Cloud\\Translate\\' => array($vendorDir . '/google/cloud-translate/src'),
    'Google\\Cloud\\Core\\' => array($vendorDir . '/google/cloud-core/src'),
    'Google\\Cloud\\' => array($vendorDir . '/google/common-protos/src/Cloud'),
    'Google\\Auth\\' => array($vendorDir . '/google/auth/src'),
    'Google\\Api\\' => array($vendorDir . '/google/common-protos/src/Api'),
    'Google\\ApiCore\\LongRunning\\' => array($vendorDir . '/google/longrunning/src/ApiCore/LongRunning'),
    'Google\\ApiCore\\' => array($vendorDir . '/google/gax/src'),
    'Garden\\Schema\\' => array($vendorDir . '/vanilla/garden-schema/src'),
    'Garden\\SafeCurl\\' => array($vendorDir . '/vanilla/safecurl/src'),
    'Garden\\Password\\' => array($vendorDir . '/vanilla/garden-password/src', $vendorDir . '/vanilla/legacy-passwords/src'),
    'Garden\\JSON\\' => array($vendorDir . '/vanilla/garden-jsont/src'),
    'Garden\\Hydrate\\' => array($vendorDir . '/vanilla/garden-hydrate/src'),
    'Garden\\Http\\' => array($vendorDir . '/vanilla/garden-http/src'),
    'Garden\\Container\\' => array($vendorDir . '/vanilla/garden-container/src'),
    'Garden\\Cli\\' => array($vendorDir . '/vanilla/garden-cli/src'),
    'Garden\\' => array($baseDir . '/library/Garden'),
    'GPBMetadata\\Google\\Type\\' => array($vendorDir . '/google/common-protos/metadata/Type'),
    'GPBMetadata\\Google\\Rpc\\' => array($vendorDir . '/google/common-protos/metadata/Rpc'),
    'GPBMetadata\\Google\\Protobuf\\' => array($vendorDir . '/google/protobuf/src/GPBMetadata/Google/Protobuf'),
    'GPBMetadata\\Google\\Longrunning\\' => array($vendorDir . '/google/longrunning/metadata/Longrunning'),
    'GPBMetadata\\Google\\Logging\\' => array($vendorDir . '/google/common-protos/metadata/Logging'),
    'GPBMetadata\\Google\\Iam\\' => array($vendorDir . '/google/common-protos/metadata/Iam'),
    'GPBMetadata\\Google\\Cloud\\Translate\\' => array($vendorDir . '/google/cloud-translate/metadata'),
    'GPBMetadata\\Google\\Cloud\\' => array($vendorDir . '/google/common-protos/metadata/Cloud'),
    'GPBMetadata\\Google\\Api\\' => array($vendorDir . '/google/common-protos/metadata/Api'),
    'GPBMetadata\\ApiCore\\' => array($vendorDir . '/google/gax/metadata/ApiCore'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Fig\\EventDispatcher\\' => array($vendorDir . '/fig/event-dispatcher-util/src'),
    'Faker\\' => array($vendorDir . '/fakerphp/faker/src/Faker'),
    'Delight\\Http\\' => array($vendorDir . '/delight-im/http/src'),
    'Delight\\Cookie\\' => array($vendorDir . '/delight-im/cookie/src'),
    'Cron\\' => array($vendorDir . '/dragonmantank/cron-expression/src/Cron'),
);
