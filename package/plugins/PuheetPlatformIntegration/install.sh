#!/bin/bash
set -e

sudo rm -rf /var/www/forum/plugins/PuheetPlatformIntegration/
sudo mkdir /var/www/forum/plugins/PuheetPlatformIntegration/
sudo cp -r * /var/www/forum/plugins/PuheetPlatformIntegration/

sudo chown -R www-data:www-data /var/www/forum
sudo rm -rf /var/www/forum/cache/*
sudo service nginx reload
