<?php

class PuheetThemeHooks extends Gdn_Plugin {

    /**
     * Creates a controller for /getGalleryPage endpoint that is used to retrieve category gallery pages.
     */
    public function rootController_getGalleryPage_Create($sender, $args) {

        if ($_SERVER["REQUEST_METHOD"] !== "GET") {
            http_response_code(403);
            return print_r("Forbidden");
        }


        if (!isset($_REQUEST["page"]) || !isset($_REQUEST["category"])) {
            http_response_code(400);
            return print_r("Bad request");
        }

        $categoryObject = $this->getCategoryByName($_REQUEST["category"]);

        if (!isset($categoryObject)) {
            http_response_code(400);
            return print_r("Bad request");
        }

        $page = intval(filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT));
        $this->setGalleryData($sender, $page, $categoryObject);

        http_response_code(200);
        header('Content-type: application/json');
        return print_r($sender->Data["CategoryGallery"]); // setGalleryData encodes gallery data, so no need to json_encode here
    }

    private function getCategoryByName($categoryName) {
        $categories = c('Themes.CategoryGalleries');

        for ($i = 0; $i < count($categories); $i++) {
            if ($categories[$i]["Name"] === $categoryName) {
                return $categories[$i];
            }
        }

        return null;
    }

    /**
     * Vanilla's plugin hook. Fires before anything is rendered.
     */
    public function base_render_before($sender)
    {
        $sender->setData('IsSignedIn', Gdn::session()->UserID > 0);
        $categoryGallery = $this->getCategoryGallery();

        if (inSection('Dashboard')) {
            // Do nothing in admin view
            return;
        } else if (isset($categoryGallery)) {
            $this->setGalleryData($sender, 1, $categoryGallery);
        } else {
            $this->setFeaturedCategories($sender);
            $this->setHomePageBanner($sender);
        }
    }

    /**
    * Adds delete icon for riche editor post when creating new post or editing existing post
    */
    public function base_render_after()
    {
        $headers = headers_list(); // get list of headers
        foreach($headers as $index => $value) {
            list($key, $value) = explode(': ', $value);

            unset($headers[$index]);

            $headers[$key] = $value;
        }

        $header = $headers['Content-Type'];

        if(($header == "application/json; charset=utf-8") == false) {

            if (strpos($_SERVER['REQUEST_URI'], 'post/editdiscussion/') !== false || strpos($_SERVER['REQUEST_URI'], 'post/discussion/') !== false) {
                echo "<script>
                        window.onload = checkImages

                        function checkImages() {
                            const a = document.querySelectorAll('div.richEditor.js-isMounted')[0]
                            const observer = new MutationObserver(function (mutations) {
                                mutations.forEach(function (mutation) {
                                    const nodes = Array.prototype.slice.call(mutation.addedNodes)
                                    nodes.forEach(function (node) {
                                        if (node.id !== 'deleteIcon' || node.classList.contains('u-excludeFromPointerEvents')) {
                                            const btnCont = node.childNodes[0].childNodes[0]
                                            if (/Android|webOS|iPhone|iPad|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                                btnCont.style.display = 'flex'
                                                btnCont.style.flexDirection = 'row'
                                            }
                                            const btnClass = btnCont.childNodes[0].className
                                            const button = document.createElement('button')
                                            button.setAttribute('id', 'deleteIcon')
                                            button.classList.add(btnClass)
                                            button.addEventListener('click', function () {
                                                node.parentNode.parentNode.remove()
                                            })
                                            const svg = createTrashSvg()
                                            button.appendChild(svg)
                                            btnCont.appendChild(button)
                                        }
                                    })
                                })
                            })
                            observer.observe(a, {
                                childList: true,
                                subtree: true,
                                attributes: false,
                                characterData: false,
                            })
                        }

                        function createTrashSvg() {
                            const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
                            svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg')
                            svg.setAttributeNS(null, 'viewBox', '0 0 22 22')
                            svg.setAttributeNS(null, 'aria-hidden', 'true')
                            svg.style.width = '36px'
                            svg.style.height = '36px'
                            svg.style.marginLeft = '6px'
                            svg.style.paddingTop = '6px'
                            const g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
                            g.setAttributeNS(null, 'clip-rule', 'evenodd')
                            g.setAttributeNS(null, 'fill-rule', 'evenodd')
                            g.setAttributeNS(null, 'fill', 'currentColor')
                            const path1 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
                            path1.setAttribute('d', 'M14,4v9c0,1.1-0.9,2-2,2H5c-1.1,0-2-0.9-2-2V4H2.3C2.1,4,2,3.9,2,3.7V3.3C2,3.1,2.1,3,2.3,3h3.2l0.3-1 C5.9,1.4,6.4,1,7,1h3c0.6,0,1.1,0.4,1.2,1l0.3,1h3.2C14.9,3,15,3.1,15,3.3v0.4C15,3.9,14.9,4,14.7,4H14z M7,2.2C7,2.1,7.1,2,7.2,2 h2.6C9.9,2,10,2.1,10,2.2L10.2,3H6.8L7,2.2z M4,4h9v9c0,0.5-0.4,1-1,1H5c-0.6,0-1-0.5-1-1V4z')
                            const path2 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
                            path2.setAttribute('d', 'M8.5,5.5L8.5,5.5C8.8,5.5,9,5.7,9,6v6c0,0.3-0.2,0.5-0.5,0.5l0,0C8.2,12.5,8,12.3,8,12V6 C8,5.7,8.2,5.5,8.5,5.5z')
                            const path3 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
                            path3.setAttribute('d', 'M10.5,5.5L10.5,5.5C10.8,5.5,11,5.7,11,6v6c0,0.3-0.2,0.5-0.5,0.5l0,0c-0.3,0-0.5-0.2-0.5-0.5V6 C10,5.7,10.2,5.5,10.5,5.5z')
                            const path4 = document.createElementNS('http://www.w3.org/2000/svg', 'path')
                            path4.setAttribute('d', 'M6.5,5.5L6.5,5.5C6.8,5.5,7,5.7,7,6v6c0,0.3-0.2,0.5-0.5,0.5l0,0C6.2,12.5,6,12.3,6,12V6 C6,5.7,6.2,5.5,6.5,5.5z')
                            g.appendChild(path1)
                            g.appendChild(path2)
                            g.appendChild(path3)
                            g.appendChild(path4)
                            svg.appendChild(g)

                            return svg
                        }
                        </script>";
            }
        }
    }

    /**
     * Finds an image url from a posted discussion.
     */
    function getImageUrl($format, $text)
    {
        if ($format == 'Markdown') {
            // find markdown image url from post body, format:
            // "![](http://localhost/vanilla/uploads/editor/uc/y6s7x40yayo5.jpg "") *Thi..."
            preg_match('/!\[[^\[\]]*\]\((.*) +"[^"]*"\)/', $text, $matches);
            return $matches[1];
        } else if ($format == 'Rich') {
            $jsonText = json_decode($text);
            $jsonTextLength = count($jsonText);

            for ($i = 0; $i < $jsonTextLength; $i++) {
                if (gettype($jsonText[$i]?->insert) !== "object") {
                    continue;
                }
                $loaderDataType = (
                    $jsonText[$i]?->insert?->{"embed-external"}?->loaderData?->type
                );
                $loaderDataTypeIsImageOrLink = (
                    $loaderDataType === "image"
                    || $loaderDataType === "link"
                );
                if (
                    $loaderDataTypeIsImageOrLink
                    && isset($jsonText[$i]?->insert?->{"embed-external"}?->data)
                ) {
                    return $jsonText[$i]->insert->{"embed-external"}->data->url;
                }
            }
            return null;
        }
    }
    /**
    * Exactly like function getImageUrl but this finds and returns up to three images per discussion
    * Need comes from SAHA Training where home page banner uses three images (desktop, tablet, mobile)
    */
    function getImageUrls($format, $text)
    {
        if ($format == 'Markdown') {
            // find markdown image url from post body, format:
            // "![](http://localhost/vanilla/uploads/editor/uc/y6s7x40yayo5.jpg "") *Thi..."
            preg_match('/!\[[^\[\]]*\]\((.*) +"[^"]*"\)/', $text, $matches);
            return $matches[1];
        } else if ($format == 'Rich') {
            $jsonText = json_decode($text);
            $jsonTextLength = count($jsonText);
            $imageUrls = array();

            for ($i = 0; $i < $jsonTextLength; $i++) {
                if (gettype($jsonText[$i]?->insert) !== "object") {
                    continue;
                }
                $loaderDataType = (
                    $jsonText[$i]?->insert?->{"embed-external"}?->loaderData?->type
                );
                $loaderDataTypeIsImageOrLink = (
                    $loaderDataType === "image"
                    || $loaderDataType === "link"
                );
                if (
                    $loaderDataTypeIsImageOrLink
                    && isset($jsonText[$i]?->insert?->{"embed-external"}?->data)
                ) {
                    array_push(
                        $imageUrls,
                        $jsonText[$i]->insert->{"embed-external"}->data->url,
                    );
                    if (sizeof($imageUrls) >= 3) {
                        break;
                    }
                }
            }
            return $imageUrls;
        }
    }

    /**
     * Gets the name of the current category gallery if the gallery feature is enabled for the current category.
     * To enable a gallery for a category add its name and gallery title to config.php like:
     * $Configuration['Themes']['CategoryGalleries'] = array ( 0 => [ 'Name' => 'Category Name A', 'Title' => 'Newest A images'], 1 => [ 'Name' => 'Category name B', 'Title' => 'Category B images'] )
     * Note that the gallery name is case sensitive.
     */
    private function getCategoryGallery() {
        $categoryGalleries = c('Themes.CategoryGalleries');

        if (
          empty($categoryGalleries)
          || count($categoryGalleries) === 0
        ) {
            return null;
        }

        for ($i = 0; $i < count($categoryGalleries); $i++) {
            $categoryName = strtolower($categoryGalleries[$i]["Name"]);
            $category = str_replace(" ", "-", $categoryName);
            $categoryWithGallery = $category . " gallery-enabled";
            $categoryWithGalleryAndFeatured = $category . " gallery-enabled featured-category";
            if (inSection("Category-$category") || inSection("Category-$categoryWithGallery") || inSection("Category-$categoryWithGalleryAndFeatured")) {
                return $categoryGalleries[$i];
            }
        }

        return null;
    }

    /**
     * Assembles data that is used by category galleries in gallery templates.
     */
    private function setGalleryData($sender, $page, $categoryGallery) {
        $pageSize = 8; // edit this to change how many pics are retrieved at a time
        $categoryId = CategoryModel::instance()->getWhere(["Name" => $categoryGallery["Name"]], "", "", 1)->resultArray()[0]["CategoryID"];

        $galleryOffset = ($page - 1 ) * $pageSize;
        $categoryGalleryDiscussionData = DiscussionModel::instance()->getWhere(["CategoryID" => $categoryId], "", "asc", $pageSize, $galleryOffset)->resultArray();
        $categoryGalleryData = [];

        for ($i = 0; $i < count($categoryGalleryDiscussionData); $i++) {
            $discussion = $categoryGalleryDiscussionData[$i];
            $imageUrl = $this->getImageUrl($discussion["Format"], $discussion["Body"]);

            if (isset($imageUrl)) {
                $recipe = [
                    "Photo" => $imageUrl,
                    "Url" => $discussion["Url"],
                    "Title" => $discussion["Name"],
                ];

                $categoryGalleryData[] = $recipe;
            }
        }

        // To decide whether the gallery is shown or not
        $categoryNameUrl = str_replace(" ", "-", strtolower($categoryGallery["Name"]));
        $isCategoryGalleryMainPage = $sender->SelfUrl === "categories/$categoryNameUrl";

        $sender->setData('CategoryGallery', json_encode($categoryGalleryData));
        $sender->setData('CategoryGalleryPageSize', $pageSize);
        $sender->setData('isCategoryGalleryMainPage', $isCategoryGalleryMainPage);
        $sender->setData('CategoryName', $categoryGallery["Name"]);
        $sender->setData('CategoryTitle', $categoryGallery["Title"]);
    }

    /**
     * Assembles data that is used by featured categories in gallery templates.
     */
    private function setFeaturedCategories($sender) {

        $allTags = TagModel::instance()->getWhere()->resultArray();
        $tagIdToName = array();
        foreach ($allTags as $t) {
            $tId = $t['TagID'];
            $tagIdToName[$tId] = $t['Name'];
        }

        $categories = CategoryModel::instance()->getFull();
        $featuredCategories = array();

        foreach ($categories as $category) {
            if ($category->Name && $category->Url && $category->PhotoUrl) {
                $c = array(
                  'Name' => $category->Name,
                  'Url' => $category->Url,
                  'PhotoUrl' => $category->PhotoUrl
                );
                array_unshift($featuredCategories, $c);
            }
        }

        $sender->setData(
          'FeaturedCategories',
          array_reverse(array_slice($featuredCategories, 0, 4)),
        );
        $discussions = DiscussionModel::instance()->getWhere(
          [
            "d.Announce" => "all",
            "d.Tags" ?? [] => "Invoke_Puheet_Featured" ?? []
          ],
          "",
          "",
          100,
        );

        $featuredDiscussions = array();
        foreach ($discussions as $discussion) {
            if (
              $discussion->Name
              && $discussion->Url
              && $discussion->Body
              && $discussion->Tags
              && (
                $discussion->Format == 'Markdown'
                || $discussion->Format == 'Rich'
              )
            ) {

                $tags = explode(",", $discussion->Tags);
                // Tag value can be id or name. Translate id to name.
                $tagNames = array();
                foreach ($tags as $t) {
                    if (is_numeric($t) && $tagIdToName[$t]) {
                        array_push($tagNames, $tagIdToName[$t]);
                    } else {
                        array_push($tagNames, $t);
                    }
                }
                $tags = $tagNames;

                $imageUrl = $this->getImageUrl(
                  $discussion->Format,
                  $discussion->Body,
                );
                if (in_array("featured", $tags) && $imageUrl) {
                    $tag = '';
                    foreach ($tags as $t) {
                        if ($t != 'featured') {
                            $tag = $t;
                            break;
                        }
                    }
                    $d = array(
                      'Name' => $discussion->Name,
                      'Url' => $discussion->Url,
                      'Tag' => $tag,
                      'PhotoUrl' => $imageUrl,
                    );
                    array_push($featuredDiscussions, $d);
                    if (sizeof($featuredDiscussions) >= 3) break;
                }
            }
        }
        $sender->setData('FeaturedDiscussions', $featuredDiscussions);
        $sender->setData(
          'PlatformUrl',
          c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl'),
        );
    }

    /**
     * Edits the WHERE clause of banner discussions' and featured categories' SQL query.
     */
    public function DiscussionModel_BeforeGet_handler($sender, $args) {
        if (
          !empty($args["Wheres"]["d.Tags"])
          && $args["Wheres"]["d.Tags"] === "Invoke_Puheet_Featured"
        ) {

            $newWheres = [];

            foreach ($args["Wheres"] as $key => $value) {
                if (!($key === "d.Tags" && $value === "Invoke_Puheet_Featured")) {
                    $newWheres[$key] = $value;
                }
            }

            $tagId = null;
            $featuredTags = TagModel::instance()->getWhere(
                ["Name" => "featured"], "", "", 1
            )->resultArray()[0] ?? [];
            $tagId = $featuredTags["TagID"] ?? null;

            $args["Wheres"] = $newWheres;

            if (isset($tagId)) {
                // searching for 'featured' or featured category's ID '3'
                $sender->SQL->like('d.Tags', 'featured', 'both');
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', "$tagId", 'none'); // 3
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', "$tagId,", 'right'); // 3,33
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', ",$tagId", 'left'); // 33,3
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', ",$tagId,", 'both'); // 33,3,33
            }

            $args["OrderBy"] = [];
            // $args["OrderBy"]["d.DateUpdated"] = "desc"; could be enabled if Vanilla saved a non-null value to DateUpdated when a new discussion is inserted
            $args["OrderBy"]["d.DateInserted"] = "desc";
        } elseif (
          !empty($args["Wheres"]["d.Tags"])
          && $args["Wheres"]["d.Tags"] === "Invoke_Puheet_Banner"
        ) {

            $newWheres = [];

            foreach ($args["Wheres"] as $key => $value) {
                if (!($key === "d.Tags" && $value === "Invoke_Puheet_Banner")) {
                    $newWheres[$key] = $value;
                }
            }

            $tagId = null;
            $bannerTags = TagModel::instance()->getWhere(
                ["Name" => "banner"], "", "", 1
            )->resultArray()[0] ?? [];
            $tagId = $bannerTags["TagID"] ?? null;

            $args["Wheres"] = $newWheres;

            if (isset($tagId)) {
                // searching for 'featured' or featured category's ID '3'
                $sender->SQL->like('d.Tags', 'banner', 'both');
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', "$tagId", 'none'); // 3
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', "$tagId,", 'right'); // 3,33
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', ",$tagId", 'left'); // 33,3
                $sender->SQL->orOp();
                $sender->SQL->like('d.Tags', ",$tagId,", 'both'); // 33,3,33
            }

            $args["OrderBy"] = [];
            // $args["OrderBy"]["d.DateUpdated"] = "desc"; could be enabled if Vanilla saved a non-null value to DateUpdated when a new discussion is inserted
            $args["OrderBy"]["d.DateInserted"] = "desc";
        }
    }

    /**
    * Finds the latest discussion with banner tag and uses its images in home page banner
    */
    private function setHomePageBanner($sender) {

        $allTags = TagModel::instance()->getWhere()->resultArray();
        $tagIdToName = array();
        foreach ($allTags as $t) {
            $tId = $t['TagID'];
            $tagIdToName[$tId] = $t['Name'];
        }

        $discussions = DiscussionModel::instance()->getWhere(
          [
            "d.Announce" => "all",
            "d.Tags" ?? [] => "Invoke_Puheet_Banner" ?? []
          ],
          "",
          "",
          100,
        );
        $homePageBanners = array();
        foreach ($discussions as $discussion) {
            if ($discussion->Name && $discussion->Url && $discussion->Body && $discussion->Tags &&
                ($discussion->Format == 'Markdown' || $discussion->Format == 'Rich')) {

                $d = array();
                $imageUrls = $this->getImageUrls($discussion->Format, $discussion->Body);

                if (count($imageUrls) == 3) {
                    $d = array('Desktop' => $imageUrls[0], 'Tablet' => $imageUrls[1], 'Mobile' => $imageUrls[2]);
                } elseif (count($imageUrls) == 2) {
                    $d = array('Desktop' => $imageUrls[0], 'Tablet' => $imageUrls[1], 'Mobile' => $imageUrls[1]);
                } elseif (count($imageUrls) == 1) {
                    $d = array('Desktop' => $imageUrls[0], 'Tablet' => $imageUrls[0], 'Mobile' => $imageUrls[0]);
                }

                if (count($imageUrls) >= 1) {
                    $bannerHref = $this->getBannerHref($discussion->Format, $discussion->Body);
                    $d['Href'] = $bannerHref;
                }

                array_push($homePageBanners, $d);
                if (sizeof($homePageBanners) >= 1) break;
            }
        }

        $sender->setData('HomePageBanners', $homePageBanners);
        $sender->setData('PlatformUrl', c('Plugins.PuheetPlatformIntegration.PuheetPlatformBackendUrl'));
    }

    /**
    * finds and return first URL from text parts of discussion. URL must start with http or https.
    */
    function getBannerHref($format, $text)
    {
        if ($format == 'Rich') {
            $jsonText = json_decode($text);
            $jsonTextLength = count($jsonText);
            for ($i = 0; $i < $jsonTextLength; $i++) {
                if (
                    isset($jsonText[$i]->insert) &&
                    !isset($jsonText[$i]->insert->{"embed-external"})
                ) {
                    $textLines = explode("\n", $jsonText[$i]->insert);
                    foreach ($textLines as $line) {
                        preg_match('/\b(?:https?:\/\/)(?:(?i:[a-z\-]+\.)+)[^\s,]+\b/', $line, $matches);
                        if (count($matches) > 0) {
                            return $matches[0];
                        }
                    }
                }
            }
        return "";
        }
    }
}
