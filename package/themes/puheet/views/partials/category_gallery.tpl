{* Supports translations for keys "Connection error" and whatever is set as $CategoryTitle *}
<h1 class="category-heading">{t c=$CategoryTitle}</h1>
<div class="category-gallery-container">{* Gallery content is rendered here *}</div>
<div class="category-navigation">
    <div class="chevron left chevron-disabled" onclick="getCategoryGalleryPage('left')"></div>
    <div class="chevron right" onclick="getCategoryGalleryPage('right')"></div>
</div>
<script>
    let loadedPages = 1;
    let galleryPageNumber = 1;
    let galleryLoadEnabled = true;
    const galleryPageSize = parseInt('{$CategoryGalleryPageSize}');

    function getCategoryGalleryPage(direction) {
        if (direction === 'left') {

            if (galleryPageNumber !== 1) {
                galleryPageNumber--;
                toggleChevron('left', true);
            } else {
                toggleChevron('left', false);
            }

            toggleChevron('right', true);
            showGalleryPage();
        } else if (direction === 'right') {
            galleryPageNumber++;

            if (galleryPageNumber > loadedPages && galleryLoadEnabled) {
                loadNewCategoryGalleryPage();
            } else if (galleryLoadEnabled) {
                showGalleryPage();
            } else {
                galleryPageNumber--;
            }
        }
    }

    function showGalleryPage() {
        const galleryElements = document.querySelectorAll('.category-gallery-item');
        const visibleRangeStart = (galleryPageNumber - 1) * galleryPageSize;
        const visibleRangeEnd = visibleRangeStart + galleryPageSize - 1;

        const galleryMaxSize = Math.ceil(galleryElements.length / galleryPageSize) * galleryPageSize;

        if ((galleryPageNumber * galleryPageSize) > galleryMaxSize) {
            galleryPageNumber--;
            return;
        }

        for (let i = 0; i < galleryElements.length; ++i) {
            if (i >= visibleRangeStart && i <= visibleRangeEnd) {
                galleryElements[i].classList.remove('category-gallery-hidden');
                galleryElements[i].classList.add('fade-in');
            } else {
                galleryElements[i].classList.add('category-gallery-hidden');
                galleryElements[i].classList.remove('fade-in');
            }
        }
    }

    function loadNewCategoryGalleryPage() {
        togglegalleryLoadEnabled(false);
        // this URL supports now only new domain schema. If using old and need to upgrade this theme somehow, it'll broke
        const galleryUrl = 'https://' + window.location.host + '/forum/getGalleryPage?page=' + galleryPageNumber + '&category=' + '{$CategoryName}';
        const request = new XMLHttpRequest();

        request.open('GET', galleryUrl, true);

        request.onload = function () {
            if (request.status >= 200 && request.status < 400) {
                const response = request.responseText;
                const galleryPage = JSON.parse(response);

                if (galleryPage.length) {
                    loadedPages++;
                    addPageToDom(galleryPage);
                    toggleChevron('right', true);
                    toggleChevron('left', true);
                } else {
                    // Reached the end of gallery pages
                    galleryPageNumber--;
                    toggleChevron('right', false);
                }
                showGalleryPage();

            } else {
                toggleChevron('right', true);
                toggleChevron('left', true);
                galleryPageNumber--;
                showError();
            }
        };
        request.send();
    }

    function addPageToDom(pageContent) {
        const container = document.querySelector('.category-gallery-container');

        for (let i = 0; i < pageContent.length; i++) {
            const galleryItem = document.createElement('a');
            galleryItem.setAttribute('class', 'category-gallery-item category-gallery-hidden');
            galleryItem.setAttribute('href', pageContent[i].Url);
            galleryItem.setAttribute('title', pageContent[i].Title);

            {literal}
            galleryItem.innerHTML = `
            <h4 class="category-item-title">${pageContent[i].Title}</h4>
            <div class="category-image-container">
                <img class="category-gallery-hidden-img" src="" alt="Discussion photo">
                <div class="gallerySpinner">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>`;

            const imageElement = galleryItem.children[1].children[0];
            loadImage(imageElement, pageContent[i].Photo);
            {/literal}

            container.append(galleryItem);
        }
    }

    function loadImage(element, src) {
        const img = new Image();

        img.onload = function() {
            element.src = img.src;
            element.parentElement.children[1].classList.add('gallerySpinner-hidden');
            element.classList.remove('category-gallery-hidden-img');
            element.classList.add('fade-in');
        };

        img.src = src;
    }

    function showError() {
        const galleryElements = document.querySelectorAll('.category-gallery-item');

        for (let i = 0; i < galleryElements.length; ++i) {
            galleryElements[i].classList.add('category-gallery-hidden');
        }

        document.querySelector('.category-gallery-container').innerHTML = '<div class="category-error">{t c="Connection error"}</div>';
    }

    function toggleChevron(direction, enable) {
        {literal}
        const chevron = document.querySelector(`.category-navigation > .chevron.${direction}`);
        {/literal}

        if (enable) {
            chevron.classList.remove('chevron-disabled');
            if (direction === 'right') {
                togglegalleryLoadEnabled(true)
            }
        } else {
            chevron.classList.add('chevron-disabled');

            if (direction === 'right') {
                togglegalleryLoadEnabled(false)
            }
        }
    }

    function togglegalleryLoadEnabled(isEnabled) {
        galleryLoadEnabled = isEnabled;
    }

    {* Rendering the first gallery page *}
    addPageToDom({$CategoryGallery});
    showGalleryPage();
</script>
<style>
    {*
    * Prefixed by https://autoprefixer.github.io
    * PostCSS: v7.0.29,
    * Autoprefixer: v9.7.6
    * Browsers: last 8 version
    *}

    .category-heading {
        width: 100%;
    }

    .category-gallery-hidden-img {
        display: none;
    }

    .category-gallery-hidden {
        display: none;
    }

    .category-gallery-container {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        width: 100%;
        max-width: 100%;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        min-height: 30em;
        align-content: start;
        -ms-flex-line-pack: start;
    }

    .category-gallery-item {
        -ms-flex-preferred-size: 50%;
        flex-basis: 50%;
        max-width: 50%;
        padding-left: 0.5em;
        padding-right: 0.5em;
    }

    .category-gallery-item:hover {
        cursor: pointer;
    }

    .category-item-title {
        width: 100%;
        white-space: nowrap;
        overflow: hidden;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        margin-top: 0.5em;
        font-size: 18px;
    }

    .category-image-container > img {
        width: 100%;
        height: 5em;
        -o-object-fit: cover;
        object-fit: cover;
        border-radius: 4px;
        -webkit-box-shadow: 2px 3px 9px 0 #0000007a;
        box-shadow: 2px 3px 9px 0 #0000007a;
    }

    .category-navigation {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        width: 100%;
    }

    .category-navigation > div {
        padding: 1em;
        cursor: pointer;
    }

    .category-navigation > div:hover {
        color: black;
    }

    .chevron::before {
        border-style: solid;
        border-width: 0.25em 0.25em 0 0;
        content: '';
        display: inline-block;
        height: 0.45em;
        left: 0.15em;
        position: relative;
        top: 0.15em;
        -webkit-transform: rotate(-45deg);
        -ms-transform: rotate(-45deg);
        transform: rotate(-45deg);
        vertical-align: top;
        width: 0.45em;
    }

    .chevron.right:before {
        left: 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }

    .chevron.left:before {
        left: 0.25em;
        -webkit-transform: rotate(-135deg);
        -ms-transform: rotate(-135deg);
        transform: rotate(-135deg);
    }

    .chevron.left.chevron-disabled, .chevron.right.chevron-disabled {
        opacity: 0.5;
    }

    .chevron.left.chevron-disabled:hover, .chevron.right.chevron-disabled:hover {
        cursor: not-allowed;
    }

    .category-error {
        color: darkred;
        padding-top: 2em;
    }

    @media only screen and (min-width: 600px) and (max-width: 899px) {
        .category-gallery-item {
            -ms-flex-preferred-size: 33%;
            flex-basis: 33%;
            max-width: 33%;
        }

        category-image-container > img, .category-image-container > .gallerySpinner {
            height: 10em;
        }

        .category-gallery-container {
            min-height: 40em;
        }

        .category-item-title {
            margin-top: 1em;
            font-size: 20px;
        }
    }

    @media only screen and (min-width: 900px) {
        .category-gallery-item {
            -ms-flex-preferred-size: 25%;
            flex-basis: 25%;
            max-width: 25%;
        }

        .category-image-container > img, .category-image-container > .gallerySpinner {
            height: 10em;
        }

        .category-gallery-container {
            min-height: 27em;
        }

        .category-item-title {
            margin-top: 1em;
            font-size: 20px;
        }
    }

    {* Spinner *}
    .gallerySpinner {
        height: 5em;
        display: flex;
        align-items: center;
        justify-content: center;
        -webkit-box-shadow: 2px 3px 9px 0 #0000007a;
        box-shadow: 2px 3px 9px 0 #0000007a;
        border-radius: 4px;
    }

    .gallerySpinner-hidden {
        display: none;
    }

    .lds-ring {
        display: inline-block;
        position: relative;
        width: 40px;
        height: 40px;
    }
    .lds-ring div {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 32px;
        height: 32px;
        margin: 4px;
        border: 4px solid rgb(255, 79, 79);
        border-radius: 50%;
        -webkit-animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: rgb(255, 79, 79) transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        -webkit-animation-delay: -0.45s;
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        -webkit-animation-delay: -0.3s;
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        -webkit-animation-delay: -0.15s;
        animation-delay: -0.15s;
    }
    @-webkit-keyframes lds-ring {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes lds-ring {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }

    {*
     * Generated by Animista on 2020-5-14 15:9:42
     * Licensed under FreeBSD License.
     * See http://animista.net/license for more info.
     * w: http://animista.net, t: @cssanimista
    *}
    .fade-in {
        -webkit-animation: fade-in 0.6s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
        animation: fade-in 0.6s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
    }

    @-webkit-keyframes fade-in {
        0% {
            opacity: 0.6;
        }
        100% {
            opacity: 1;
        }
    }
    @keyframes fade-in {
        0% {
            opacity: 0.6;
        }
        100% {
            opacity: 1;
        }
    }

</style>
