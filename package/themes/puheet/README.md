#Puheet Theme for Vanilla Forums

##Styling
Styles are made with plain CSS. Although Vanilla's own documentation might mention that themes need to be built, this only refers to the SASS files in `.src/scss`. The SASS files are not used in Puheet theme, so there's also no need to run any build scripts.  

`./design/custom.css` contains the styles for the forum.
 
`./design/customadmin.css` contains the styles for the admin pages.

**Since the styling is made with plain CSS, there's no autoprefixer! You'll need to prefix any CSS you've written yourself or by using e.g. https://autoprefixer.github.io/**

##Features
This section lists features that have been implemented in this theme.

###Image gallery for categories
Adds an image gallery on the top part of a category page. The images in the gallery are retrieved from posts in the category.

***How to use:***
1. To enable a gallery for a category add its name and gallery title to config.php like:
    ```
    $Configuration['Themes']['CategoryGalleries'] = array (
         0 => [ 'Name' => 'Category Name A', 'Title' => 'Newest A images'],
         1 => [ 'Name' => 'Category name B', 'Title' => 'Category B images']
    );
    ```
    Title can be pretty much anything, but name must be the exact gallery name.

2. Next make sure the category URL is similar to the gallery name. E.g. if the name is Puheet Image Gallery, the URL should be puheet-image-gallery.

3. Then make sure that the category has one of the following CSS class combinations, when the gallery's name is Test Images:
    * Category-test-images
    * Category-test-images gallery-enabled
    * Category-test-images gallery-enabled featured-category
    
    Category names that have spaces might or might not work. This should be tested and possibly fixed.

*Note that the gallery names are case sensitive!*

### Home Page Banner

Adds Banner to the top of home page. Styling for image width, height etc. could be written to views/partials/home_page_banner.tpl file or in design/custom.css file.

***How it works:***
* Functionality is copied from featured tag
* It looks for discussions which has banner tag.
* It uses the latest discussion with banner tag as home page banner
* Discussion can have 1-3 images.
   * Uppermost image is used for desktop
   * Middle one is used for tablets
   * Undermost image is used for mobile phones
   * If discussion contains two images, upper is used for desktop and lower to rest
   * If discussion has one image it is used for every device
* Discussion can contain text for making things cleaner but text is not included to banner.
* Banner can contain link to certain page (customer campaign page, to forums gallery, to certain survey etc.)
    * plugin searches strings from discussion's text part that starts with https or http.
    * It uses the first string that match, rest are ignored. If none found then link points to home page.

### SearchBox
* This plugin hides Vanilla's searchbox by default
* Searchbox can be enabled by removing comments from default.master.tpl

### Edit Profile View
* This plugin hides Email input field and checkbox for "Allow other members to see your email?"
* edit.php does the rendering of wanted form fields. Vanilla's core application/dashboard/views/profile/edit.php
is copied to this plugin's profile dir and email fields are removed from it.
* If there's need to use the default edit.php simply remove profile folder from this plugin

### Delete Icon for Rich Text Editor

Adds delete icon for image in rich text editor when creating new post or editing existing one.

***TODO:***
* There is some problems getting the icon working in desktop view. This is not that critical because image can be deleted with keyboard keys. 
